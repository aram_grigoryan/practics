function getType(data) {
    const isInteger = Number.isInteger(Number(data));

    if (isInteger) {
      return 'целый';
    }

    const floatRegexp = /^[+-]?\d+(\.\d+)$/;
    const isFloat = floatRegexp.test(data.toString());

    if (isFloat) {
      return 'вещественный';
    }

    return 'символьный';
  }

  function input() {
    const inputElement = document.getElementById('input');
    const resultElement = document.getElementById('result');

    resultElement.innerHTML = `Результат: ${getType(inputElement.value)}`;
  }